require 'test_helper'

class LicenseControllerTest < ActionDispatch::IntegrationTest
  test "should get terms" do
    get license_terms_url
    assert_response :success
  end

end
