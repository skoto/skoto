# Skoto - README
### High Res Photos and Videos for Just About Anything

Skoto is a hub of aerial photographs and videos to connect aerial photographers to event managers and other ordinary folk. The photographer pays $5/month to upload endless amounts of photos and videos. Event managers and other folks can see them, and if they like your services, they can contact you. You can photos/videos of what they like and charge them *whatever* you like per hour on your own terms.

**Language:** Ruby on Rails

**Requirements:** Ruby on Rails and Bundler **MUST** be installed on your computer for this to work properly

## Steps

**1.** Download the project as a ZIP or use GitHub Desktop

**2.** Navigate to the project using your Terminal of Preference

**3.** Run ``bundle install`` to install the necessary gems onto your system

**4.** Run ``rails server`` to start the server. Then navigate to **localhost:3000** in your browser

**5.** Make edits that you need to

**6.** Add and commit your changes to GIT and Upload to **this repository**

**This project is licensed under the MIT License**
