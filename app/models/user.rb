class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :lockable
  validates_uniqueness_of :name
  validates_uniqueness_of :bio
  validates_uniqueness_of :email
  has_many :otos, dependent: :destroy
end
