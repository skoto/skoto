class Oto < ApplicationRecord
     belongs_to :user
     has_attached_file :image, styles: { medium: "700x500#", small: "350x200#" }, default_url: "/images/:style/missing.png"
     validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
     validate :oto_count_within_limit, :on => :create
     validates_attachment_size :image, :in => 0.megabytes..10.megabytes
     attr_accessor :validation_code, :flash_notice

     def self.search(search)
          if search
               where("zipcode LIKE ?", "%#{search}%")
          else
               all
          end
     end

     def oto_count_within_limit
          if self.user.otos.count >= 25
               self.errors.add(:base, "Exceeded Otos Limit")
          end
     end
end
