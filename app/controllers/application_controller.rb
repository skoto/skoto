class ApplicationController < ActionController::Base
     protect_from_forgery with: :exception
     before_action :configure_permitted_parameters, if: :devise_controller?

     def authorized_pilot
          redirect_to root_url, alert: "You are not a photographer. This page is denied to you." if current_user.pilot == false
     end

     protected
          def configure_permitted_parameters
               devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
               devise_parameter_sanitizer.permit(:sign_in, keys: [:name])
               devise_parameter_sanitizer.permit(:sign_up, keys: [:bio])
               devise_parameter_sanitizer.permit(:sign_in, keys: [:bio])
               devise_parameter_sanitizer.permit(:sign_up, keys: [:camera])
               devise_parameter_sanitizer.permit(:sign_in, keys: [:camera])
               devise_parameter_sanitizer.permit(:sign_up, keys: [:priceph])
               devise_parameter_sanitizer.permit(:sign_in, keys: [:priceph])
               devise_parameter_sanitizer.permit(:sign_up, keys: [:airtime])
               devise_parameter_sanitizer.permit(:sign_in, keys: [:airtime])
               devise_parameter_sanitizer.permit(:sign_up, keys: [:zipcode])
               devise_parameter_sanitizer.permit(:sign_in, keys: [:zipcode])

               devise_parameter_sanitizer.permit(:sign_up) do |user_params|
                    user_params.permit(:name, :email, :bio, :camera, :priceph, :airtime, :zipcode, :password, :password_confirmation)
               end

               devise_parameter_sanitizer.permit(:account_update) do |user_params|
                    user_params.permit(:name, :bio, :camera, :priceph, :airtime, :zipcode, :current_password, :preferred)
               end
          end
end
