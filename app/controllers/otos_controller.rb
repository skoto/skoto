class OtosController < ApplicationController
     before_action :find_oto, only: [:show, :edit, :update, :destroy]
     before_action :authenticate_user_first, only: [:edit, :update, :destroy]
     before_action :authorized_pilot, only: [:edit, :update, :destroy, :profile]

     def index
          @otos = Oto.all.order("created_at ASC")
          @otos = Oto.search(params[:search])
     end

     def new
          @oto = current_user.otos.build
     end

     def create
          @oto = current_user.otos.build(oto_params)
          if @oto.save
               redirect_to @oto
          else
               alert = @oto.errors.full_messages_for(:base).to_sentence
               render "new"
          end
     end

     def profile
          @otos = Oto.where(user_id: current_user)
     end

     def show
     end

     def edit
     end

     def update
          if @oto.update(oto_params)
               redirect_to @oto
          else
               render "edit"
          end
     end

     def destroy
          if @oto.destroy
               redirect_to root_path
          else
               redirect_to @oto
          end
     end

     private
          def oto_params
               params.require(:oto).permit(:title, :description, :image).merge(zipcode: current_user.zipcode)
          end

          def find_oto
               @oto = Oto.find(params[:id])
          end

          def authenticate_user_first
               if current_user != Oto.find(params[:id]).user
                    redirect_to @oto
               else
               end
          end
end
