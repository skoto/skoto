Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  devise_for :users, controllers: {registrations: "registrations"}
  resources :otos
  resources :search

  devise_scope :user do
      get "login", to: "devise/sessions#new"
      get "signup", to: "devise/registrations#new"
      get "logout", to: "devise/sessions#destroy"
  end

  authenticated :user do
       root "otos#profile"
       get "profile", to: "otos#profile"
  end

  root "index#welcome"
  get "license/terms"
end
