class AddZipToOtos < ActiveRecord::Migration[5.0]
  def change
    add_column :otos, :zip, :decimal
  end
end
