class AddAttachmentImageToOtos < ActiveRecord::Migration
  def self.up
    change_table :otos do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :otos, :image
  end
end
