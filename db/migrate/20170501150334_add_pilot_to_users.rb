class AddPilotToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :pilot, :boolean
  end
end
