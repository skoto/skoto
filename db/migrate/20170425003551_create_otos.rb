class CreateOtos < ActiveRecord::Migration[5.0]
  def change
    create_table :otos do |t|
      t.string :title
      t.text :description
      t.boolean :donate

      t.timestamps
    end
  end

  def up
       remove_column :donate
  end
end
