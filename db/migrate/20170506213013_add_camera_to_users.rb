class AddCameraToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :camera, :string
    add_column :users, :airtime, :string
  end
end
