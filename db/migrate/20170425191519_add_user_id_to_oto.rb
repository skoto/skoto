class AddUserIdToOto < ActiveRecord::Migration[5.0]
  def change
    add_column :otos, :user_id, :integer
  end
end
