class AddZipcodeToOtos < ActiveRecord::Migration[5.0]
  def change
    add_column :otos, :zipcode, :string
  end
end
